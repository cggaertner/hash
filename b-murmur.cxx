/*	Copyright 2012 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "hash.h"
#include "MurmurHash3.h"

#include <cstdio>
#include <ctime>

class Timer {
	unsigned &out_;
	std::clock_t start_;
	std::clock_t end_;

public:
	Timer(unsigned &out) : start_(std::clock()), out_(out) {}
	~Timer() {
		end_ = std::clock();
		out_ = (unsigned)((end_ - start_) * 1000 / CLOCKS_PER_SEC);
	}
};

int main() {
	enum { SIZE = 4091, COUNT = 500000, SEED = 0xDEADBEEF };

	unsigned char *key = new unsigned char[SIZE];
	for(unsigned i = 0; i < SIZE; ++i)
		key[i] = i % 42;

	unsigned result;

	{
		Timer timer(result);
		uint32_t dummy = 1, hash;
		for(unsigned i = COUNT; i--; dummy *= hash)
			MurmurHash3_x86_32(key, SIZE, SEED, &hash);
	}
	printf("reference implementation: %ums\n", result);

	{
		Timer timer(result);
		uint32_t dummy = 1, hash;
		for(unsigned i = COUNT; i--; dummy *= hash)
			hash = murmur3_32(key, SIZE, SEED);
	}
	printf("portable implementation: %ums\n", result);

	return 0;
}
