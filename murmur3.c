/*	Copyright 2012 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
	Based on public domain code by Austin Appleby
*/

#include "hash.h"

#define C1 0xCC9E2D51
#define C2 0x1B873593
#define C3 0xE6546B64
#define C4 0x85EBCA6B
#define C5 0xC2B2AE35

static inline _Bool le32(void)
{
	return ((const union { unsigned char bytes[4]; uint32_t value; })
		{ { 1, 2, 3, 4 } }).value == 0x04030201;
}

static inline _Bool aligned32(const void *ptr)
{
	return (uintptr_t)ptr % sizeof (uint32_t) == 0;
}

static inline uint32_t rotl32(uint32_t x, int r)
{
	return (x << r) | (x >> (32 - r));
}

static inline uint32_t get32(const uint8_t *bytes, size_t count)
{
	uint32_t block = 0;

	switch(count)
	{
		case 4: block |= (uint32_t)bytes[3] << 24;
		case 3: block |= (uint32_t)bytes[2] << 16;
		case 2: block |= (uint32_t)bytes[1] << 8;
		case 1: block |= (uint32_t)bytes[0];
	}

	return block;
}

static inline uint32_t mix(uint32_t hash, uint32_t block, _Bool partial)
{
	block *= C1;
	block = rotl32(block, 15);
	block *= C2;

	hash ^= block;
	if(partial)
		return hash;

	hash = rotl32(hash, 13);
	hash = hash * 5 + C3;
	return hash;
}

static inline uint32_t burn(uint32_t hash, size_t size)
{
	hash ^= (uint32_t)size;
	hash ^= hash >> 16;
	hash *= C4;
	hash ^= hash >> 13;
	hash *= C5;
	hash ^= hash >> 16;
	return hash;
}

uint32_t murmur3_32(const void *key, size_t size, uint32_t seed)
{
	const uint8_t *tail = (const uint8_t *)key + (size / 4) * 4;
	const uint8_t *cursor = key;

	uint32_t hash = seed;

	if(le32() && aligned32(key))
	{
		for(; cursor < tail; cursor += 4)
			hash = mix(hash, *(uint32_t *)cursor, 0);
	}
	else
	{
		for(; cursor < tail; cursor += 4)
			hash = mix(hash, get32(cursor, 4), 0);
	}

	hash = mix(hash, get32(tail, size & 3), 1);
	return burn(hash, size);
}
