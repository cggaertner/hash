/*	Copyright 2012 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "hash.h"
#include "MurmurHash3.h"

#undef NDEBUG
#include <cassert>

int main() {

	{
		uint32_t seed = 42;
		const char *foo = "foo";
		uint32_t hash = 0;

		MurmurHash3_x86_32(foo, 4, seed, &hash);
		assert(hash == murmur3_32(foo, 4, seed));

		MurmurHash3_x86_32(foo + 1, 3, seed, &hash);
		assert(hash == murmur3_32(foo + 1, 3, seed));

		MurmurHash3_x86_32(foo + 2, 2, seed, &hash);
		assert(hash == murmur3_32(foo + 2, 2, seed));

		MurmurHash3_x86_32(foo + 3, 1, seed, &hash);
		assert(hash == murmur3_32(foo + 3, 1, seed));

		MurmurHash3_x86_32(foo + 4, 0, seed, &hash);
		assert(hash == murmur3_32(foo + 4, 0, seed));
	}

	{
		enum { SIZE = 4091, SEED = 0xDEADBEEF };

		unsigned char *key = new unsigned char[SIZE];
		for(unsigned i = 0; i < SIZE; ++i)
			key[i] = i % 42;

		uint32_t hash;
		MurmurHash3_x86_32(key, SIZE, SEED, &hash);
		assert(hash == murmur3_32(key, SIZE, SEED));
	}

	return 0;
}
