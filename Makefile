RM := rm -f
CC := gcc
CXX := g++
CFLAGS := -std=c99 -Wall -Wextra -Werror
CXXFLAGS := -std=c++98

COBJECTS := murmur3.o jenkins.o
CXXOBJECTS := MurmurHash3.o
TESTS := t-murmur
BENCHMARKS := b-murmur b-shootout

.PHONY : build clean check murmur-bench shootout

build : $(COBJECTS) $(CXXOBJECTS)

clean :
	$(RM) $(COBJECTS) $(CXXOBJECTS) $(TESTS) $(BENCHMARKS)

check : $(TESTS)
	@set -e; for TEST in $(sort $^); do echo ./$$TEST; ./$$TEST; done

murmur-bench : b-murmur
shootout : b-shootout
murmur-bench shootout :
	./$<

murmur3.o jenkins.o : hash.h
MurmurHash3.o : MurmurHash3.h

$(CXXOBJECTS) : %.o : %.cpp
	$(CXX) $(CXXFLAGS) -O3 -c -o $@ $<

$(COBJECTS) : %.o : %.c
	$(CC) $(CFLAGS) -O3 -c -o $@ $<

t-murmur b-murmur : % : %.cxx murmur3.o MurmurHash3.o
t-murmur b-murmur :
	$(CXX) $(CXXFLAGS) -O3 -o $@ $^

b-shootout : % : %.c murmur3.o jenkins.o
b-shootout :
	$(CC) $(CFLAGS) -O3 -o $@ $^
