/*	Copyright 2012 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#ifndef HASH_H_
#define HASH_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

uint32_t murmur3_32(const void *key, size_t size, uint32_t seed);
uint32_t one_at_a_time(const void *key, size_t size, uint32_t seed);

#ifdef __cplusplus
}
#endif

#endif
