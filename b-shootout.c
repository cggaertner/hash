#include "hash.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

enum { SIZE = 4091, COUNT = 500000, SEED = 0xDEADBEEF };

static void bench(const char *name,
	uint32_t algorithm(const void *, size_t, uint32_t), const void *key)
{
	{
		clock_t start = clock();
		uint32_t dummy = 1, hash;
		for(unsigned i = COUNT; i--; dummy *= hash)
			hash = algorithm(key, SIZE - 1, SEED);
		clock_t end = clock();
		printf("%s: %ums\n", name,
			(unsigned)((end - start) * 1000 / CLOCKS_PER_SEC));
	}

	{
		clock_t start = clock();
		uint32_t dummy = 1, hash;
		for(unsigned i = COUNT; i--; dummy *= hash)
			hash = algorithm(key + 1, SIZE - 1, SEED);
		clock_t end = clock();
		printf("%s, unaligned: %ums\n", name,
			(unsigned)((end - start) * 1000 / CLOCKS_PER_SEC));
	}
}

int main(void) {
	unsigned char *key = malloc(SIZE);
	for(unsigned i = 0; i < SIZE; ++i)
		key[i] = i % 42;

	bench("One-at-a-time", one_at_a_time, key);
	bench("MurmurHash3-32", murmur3_32, key);

	return 0;
}

