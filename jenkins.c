/*	Copyright 2012 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
	Based on public domain code by Bob Jenkins
*/

#include "hash.h"

uint32_t one_at_a_time(const void *key, size_t size, uint32_t seed)
{
	uint32_t hash = seed;

	const unsigned char *cursor = key;
	const unsigned char *end = cursor + size;
	for(; cursor < end; ++cursor)
	{
		hash += *cursor;
		hash += (hash << 10);
		hash ^= (hash >> 6);
	}

	hash += (hash << 3);
	hash ^= (hash >> 11);
	hash += (hash << 15);

	return hash;
}
